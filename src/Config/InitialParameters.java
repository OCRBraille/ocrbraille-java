package Config;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;

import Preprocesado.*;
import Reconocimiento.DivideSymbolHorizontally;
import Reconocimiento.DivideDotsHorizontally;
import Reconocimiento.DivideDotsVertically;
import Reconocimiento.DivideSymbolVertically;
import Reconocimiento.Decode;
import Reconocimiento.BinaryBrailleCodes;
import org.opencv.core.Mat;

/**
 * Clase que inicializa los pasos y las variables compartidas
 */

public class InitialParameters {

    private Mat matImage;
    private BufferedImage image;
    private int[] index; // Vector que almacena los índices que son necesarios para la posterior eliminación de bordes redundantes
    private ArrayList<Integer> symbolVertLines; // Lista con las líneas verticales que segmentan la image, delimitando cada uno de los símbolos Braille de la image
    private ArrayList<Integer> symbolHorLines; // Lista con las líneas horizontales que segmentan la image, delimitando cada uno de los símbolos Braille de la image
    private ArrayList<Integer> dotVertLines; // Lista con las líneas verticales que segmentan cada símbolo Braille. Hay una línea por cada símbolo contenido en la image
    private ArrayList<Integer> dotHorLines; // Lista con las líneas horizontales que segmentan cada símbolo Braille. Hay dos líneas por cada símbolo contenido en la image
    private int totalHorLines; // Indica el número de líneas horizontales que hemos pintado
    private ArrayList<String> brailleCodes; // Almacena el vector pratrón con el texto en código Braille que vamos a Decodificar
    private ArrayList<BufferedImage> imageRows;
    private ArrayList<BufferedImage> imageCols;

    private LoadImage loadImage;
    //    private Greyscale greyscale;
    private Binarize binarize;
    private MedianFilter medianFilter;
    private Dilate dilate;
    private Inverse inverse;
    private DetectCircles detectCircles;

    private DivideSymbolHorizontally divideSymbolHorizontally;
    private DivideSymbolVertically divideSymbolVertically;
    private DivideDotsHorizontally divideDotsHorizontally;
    private DivideDotsVertically divideDotsVertically;
    private BinaryBrailleCodes binaryBrailleCodes;
    private Decode decode;

    public InitialParameters() {

        // Inicializamos los diferentes atributos de la clase

        index = new int[4];

        for (int i = 0; i < 4; i++) {
            index[i] = 0;
        }

        totalHorLines = 0;

        symbolHorLines = new ArrayList<>();
        symbolVertLines = new ArrayList<>();
        dotVertLines = new ArrayList<>();
        dotHorLines = new ArrayList<>();
        brailleCodes = new ArrayList<>();
        imageRows = new ArrayList<>();
        imageCols = new ArrayList<>();

        loadImage = new LoadImage(this);
//		greyscale = new Greyscale(this);
        binarize = new Binarize(this);
        medianFilter = new MedianFilter(this);
        dilate = new Dilate(this);
        inverse = new Inverse(this);
        detectCircles = new DetectCircles(this);

        divideSymbolHorizontally = new DivideSymbolHorizontally(this);
        divideSymbolVertically = new DivideSymbolVertically(this);
        divideDotsHorizontally = new DivideDotsHorizontally(this);
        divideDotsVertically = new DivideDotsVertically(this);
        binaryBrailleCodes = new BinaryBrailleCodes(this);
        decode = new Decode(this);
    }

    public String doExecute(String path) {

        // preprocesado

        loadImage.doAction(path);
//        greyscale.doAction();
        binarize.doAction();
        medianFilter.doAction();
        dilate.doAction();
        inverse.doAction();
        detectCircles.doAction();
//        EliminarBordes.doAction();

        // reconociemiento
        this.setImage(this.convertImage());

        divideSymbolHorizontally.doAction();
        int rowsLength = imageRows.size();

        StringBuilder rows = new StringBuilder();
        StringBuilder text = new StringBuilder();

        for (int i = 0; i < rowsLength; i++) {

            this.setImage(this.imageRows.get(i));
            symbolHorLines.clear();

            divideSymbolVertically.doAction();

            for (BufferedImage imageCol : imageCols) {

                this.setImage(imageCol);
                symbolVertLines.clear();

                divideSymbolHorizontally.checkSpaces();

                divideDotsHorizontally.doAction();
                divideDotsVertically.doAction();
                binaryBrailleCodes.doAction();

                rows.append(decode.doAction());


                symbolHorLines.clear();

                dotVertLines.clear();
                dotHorLines.clear();
                totalHorLines = 0;
                brailleCodes.clear();

            }

            imageCols.clear();

            new UI(this).paint(this.imageRows.get(i));

            text.append(rows.toString().trim());
            if (i != rowsLength - 1)
                text.append(System.getProperty("line.separator"));
            rows.setLength(0);
        }

        return text.toString();
    }

    public String getRowsLength(String path) {

        // preprocesado

        loadImage.doAction(path);
//        greyscale.doAction();
        binarize.doAction();
        medianFilter.doAction();
        dilate.doAction();
        inverse.doAction();
        detectCircles.doAction();
//        EliminarBordes.doAction();

        // reconociemiento
        this.setImage(this.convertImage());
        divideSymbolHorizontally.doAction();

        int rowsLength = imageRows.size();
        return String.valueOf(rowsLength);
    }

    public BufferedImage convertImage() {
        return ImageUtils.matToImage(matImage);
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public int[] getIndex() {
        return index;
    }

    public void setIndex(int[] index) {
        this.index = index;
    }

    public ArrayList<Integer> getSymbolHorLines() {
        return symbolHorLines;
    }

    public void setSymbolHorLines(ArrayList<Integer> symbolHorLines) {
        this.symbolHorLines = symbolHorLines;
    }

    public ArrayList<Integer> getSymbolVertLines() {
        return symbolVertLines;
    }

    public void setSymbolVertLines(ArrayList<Integer> symbolVertLines) {
        this.symbolVertLines = symbolVertLines;
    }

    public int getTotalHorLines() {
        return totalHorLines;
    }

    public void setTotalHorLines(int totalHorLines) {
        this.totalHorLines = totalHorLines;
    }

    public ArrayList<Integer> getDotVertLines() {
        return dotVertLines;
    }

    public void setDotVertLines(ArrayList<Integer> dotVertLines) {
        this.dotVertLines = dotVertLines;
    }

    public ArrayList<Integer> getDotHorLines() {
        return dotHorLines;
    }

    public void setDotHorLines(ArrayList<Integer> dotHorLines) {
        this.dotHorLines = dotHorLines;
    }

    public ArrayList<String> getBrailleCodes() {
        return brailleCodes;
    }

    public Mat getMatImage() {
        return matImage;
    }

    public void setMatImage(Mat matImage) {
        this.matImage = matImage;
    }

    public ArrayList<BufferedImage> getImageRows() {
        return imageRows;
    }

    public ArrayList<BufferedImage> getImageCols() {
        return imageCols;
    }

    public void setImageRows(ArrayList<BufferedImage> imageRows) {
        this.imageRows = imageRows;
    }
}
