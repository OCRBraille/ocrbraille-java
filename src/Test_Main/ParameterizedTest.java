package Test_Main;

import Config.InitialParameters;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.AfterClass;
import org.opencv.core.Core;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.nio.file.InvalidPathException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

/**
 * Clase para testing automatizado de imagenes
 */
@RunWith(value = Parameterized.class)
public class ParameterizedTest {

    // Compulsory
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    private String filePath;
    private String message;
    private String rows;
    private static PrintWriter failsWriter = null;
    private static FileWriter jsonFileWriter = null;

    public static void main(String[] args) throws IOException {

//		PanelImagen panel = new PanelImagen("C:\\f.jpg");

//		// Creamos un objeto de la clase ReconocimientoBraille
//
//		ReconocimientoBraille rb = new ReconocimientoBraille();
//
//		// Creamos el mensaje a decodificar
//
//		ArrayList<String> lista = new ArrayList<String>();

        //comentar para recibir por parametro el path
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter the path to the image file:");
        String path;

        //comentar para recibir por parametro el path

        //descomentar para recibir por parametro el path
        //String path = args[0];

        try {

            //JP: Leemos el path por Consola
            path = br.readLine();

            File file = new File(path);

            if (file.isDirectory())
                throw new InvalidPathException(path, "The path is a directory");

            if (!file.exists())
                throw new InvalidPathException(path, "The file does not exist");

            //JP: Objeto inicializador necesario para construir las estructuras

            InitialParameters vi = new InitialParameters();

            //JP: Le pasamos un path -> te devuelve un String con el mensaje

            String message = vi.doExecute(path);

            // Mostramos por consola

            System.out.println(message);
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

        //descomentar para que vuelva a pedir el path
        main(null);
    }

    private static String evaluateImage(String filePath) {

        InitialParameters vi = new InitialParameters();
        String decoding = vi.doExecute(filePath);
        return decoding;
    }

    private String getRowsLength(String filePath) {

        InitialParameters vi = new InitialParameters();
        return vi.getRowsLength(filePath);
    }


    public static String removeExtension(String fileName) {

        int pos = fileName.indexOf(".");
        if (pos > 0) {
            fileName = fileName.substring(0, pos);
        }
        return fileName;
    }


    // Inject via constructor
    // for {8, 2, 10}, numberA = 8, numberB = 2, expected = 10
//    public ParameterizedTest(String filePath, String message, String rows) {
//        this.filePath = filePath;
//        this.message = message;
//        this.rows = rows;
//    }

    public ParameterizedTest(String filePath, String message) {
        this.filePath = filePath;
        this.message = message;
    }


    // name attribute is optional, provide an unique name for test
    // multiple parameters, uses Collection<Object[]>
    @Parameters
    public static Collection<Object[]> data() {

        File folder = new File("src/Imagenes");
        List<Object[]> tests = new ArrayList<>();

        try {
            PrintWriter writer = new PrintWriter("src/Test_Main/paths.txt", "UTF-8");
            for (File fileEntry : folder.listFiles()) {

                String path = fileEntry.getAbsolutePath();
                writer.println(path);
            }

            writer.close();
        } catch (UnsupportedEncodingException | FileNotFoundException e) {
            e.printStackTrace();
        }

        JSONParser jsonParser = new JSONParser();

        try {

            FileReader fileReader = new FileReader("src/Test_Main/test.json");

            JSONArray jsonArray = (JSONArray) jsonParser.parse(fileReader);

            for (Object aTestData : jsonArray) {
                JSONObject testData = (JSONObject) aTestData;
//                if(testData.get("rows") == null)
//                    tests.add(new Object[]{testData.get("path"), testData.get("decoding"), "-1"});
//                else
//                    tests.add(new Object[]{testData.get("path"), testData.get("decoding"), testData.get("rows")});
                tests.add(new Object[]{testData.get("path"), testData.get("decoding")});
            }

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        try {
            failsWriter = new PrintWriter("src/Test_Main/failed.txt", "UTF-8");
            failsWriter.println("--------------- Path for Failed Tests ---------------\n");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return tests;
    }


    @Test
    public void evaluateImageTest() {

        String decoding = evaluateImage(filePath);

        try {
            assertThat("decoding is incorrect", decoding, is(message));
            System.out.println("Test for: " + filePath + " - passed \nResult: " + decoding);
            //writer.println(filePath+"        - passed");
        } catch (AssertionError e) {
            System.out.println("Test for: " + filePath + " - failed");
            failsWriter.println("------------------------Deco-------------------------\n" + decoding);
            failsWriter.println("\n------------------------Path-------------------------\n" + filePath);
            //writer.println(filePath+"        - failed");
            throw e;
        }
    }

    @AfterClass
    public static void doYourOneTimeTeardown() {
        failsWriter.close();
    }


//    @Test
//    public void evaluateRowsTest() {
//
//        String rowsLength = getRowsLength(filePath);
//
//        try{
//            assertThat("Incorrect number of rows",rowsLength,is(rows));
//            //writer.println(filePath+"        - passed");
//        }
//        catch(AssertionError e){
////            System.out.println("Test for: "+filePath+" - failed");
//            //writer.println(filePath+"        - failed");
//            throw e;
//        }
//    }

}
