package Test_Main;

//import java.util.ArrayList;

import Config.InitialParameters;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.InvalidPathException;
//import Reconocimiento.ReconocimientoBraille;
import org.opencv.core.Core;

/**
 * Clase Main que pide el path por parametro al ejecutar
 */

public class BrailleDecodeMain {


    // Compulsory
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }


//    static {
//        String path1 = "C:\\Program Files\\Java\\opencv\\build\\java\\x64\\opencv_java2413.dll";
//        System.load(path1);
//    }

    public static void main(String[] args) throws IOException {

//		PanelImagen panel = new PanelImagen("C:\\f.jpg");

//		// Creamos un objeto de la clase ReconocimientoBraille
//
//		ReconocimientoBraille rb = new ReconocimientoBraille();
//
//		// Creamos el mensaje a decodificar
//
//		ArrayList<String> lista = new ArrayList<String>();

        //comentar para recibir por parametro el path
//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//        System.out.print("Enter the path to the image file:");
//        String path = "";
        //comentar para recibir por parametro el path

        //descomentar para recibir por parametro el path
        String path = args[0];

        try {
            //JP: Leemos el path por Consola
            //path = br.readLine();

            File file = new File(path);

            if (file.isDirectory())
                throw new InvalidPathException(path, "The path is a directory");

            if (!file.exists())
                throw new InvalidPathException(path, "The file does not exist");

            //JP: Objeto inicializador necesario para construir las estructuras

            InitialParameters vi = new InitialParameters();

            //JP: Le pasamos un path -> te devuelve un String con el mensaje

            String message = vi.doExecute(path);

            // Mostramos por consola

            System.out.println(message);
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

        //descomentar para que vuelva a pedir el path
        //main(null);
    }

}
