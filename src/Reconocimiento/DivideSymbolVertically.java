package Reconocimiento;

import java.awt.image.BufferedImage;
import java.util.Arrays;

import Config.InitialParameters;
import Preprocesado.UI;

import static Preprocesado.DetectCircles.tam;

/**
 * Clase en la que implementamos el listener que se encarga de dividir los simbolos de la
 * imagen mediante lineas verticales, delimitando los diferentes simbolos del alfabeto Braille.
 */

public class DivideSymbolVertically {

    private InitialParameters params;
    private BufferedImage image;
    private int zerosMayus;

    public DivideSymbolVertically(InitialParameters params) {
        this.params = params;
    }

    // Metodo que se encarga lineas verticales

    public void doAction() {

        image = params.getImage();

        divideSymbols();

        splitImageCols();

//		new UI(params).paint(image);
    }

    // Metodo que se encarga de dividir la imagen mediante líneas verticales, delimitando los diferentes caracteres en código Braille

    public void divideSymbols() {

        BufferedImage salida = image;

        int[] histHor = horizontalHistogram();

        int size = 0;
        int sizeAccum = tam;
        int sizeBefore = 0;
        int tamAcum = 0, tamAnt = 0;//= DecodingUtils.calculaTam(histHor,0);
        int cont = 0, cant = 1;
        int start = 1, i = 0, j = 0;
        int ceros = 0, avgCeros = 0, espacios = 0, avgEspacios = 0;
//		double ESPACIO_ENTRE_LETRAS=0.92*tam, ESPACIO_ENTRE_PUNTOS=0.87*tam;
//		double ESPACIO_ENTRE_LETRAS=0.25*tam, ESPACIO_ENTRE_PUNTOS=0.15*tam;
//		double ESPACIO_ENTRE_LETRAS=0.1*colAvgLetra+0.55*tam, ESPACIO_ENTRE_PUNTOS=0.1*colAvgPto+0.5*tam;

//		double ESPACIO_ENTRE_LETRAS=1.8*tam, ESPACIO_ENTRE_PUNTOS=1.7*tam;

        double ESPACIO_ENTRE_LETRAS = 0.8 * tam, ESPACIO_ENTRE_PUNTOS = 0.4 * tam;
//        double ESPACIO_ENTRE_LETRAS=0.5*tam, ESPACIO_ENTRE_PUNTOS=0.4*tam;
//		int zerosCount = 0;
//        for(i = 0 ; i < histHor.length ; i++) {
//
//			if (histHor[i] == 0) {
//				zerosCount = DecodingUtils.countZeros(histHor, i);
//				if(zerosCount < 4*tam) {
//					avgCeros += zerosCount;
//					j++;
//				}
//			}
//		}
//		avgCeros /= j;
//		j=0;
//
////
//		for(i = 0 ; i < histHor.length ; i++) {
//
//			if (histHor[i] == 0) {
//				espacios = DecodingUtils.countZeros(histHor, i);
//				if(espacios > 0.1 * avgCeros && espacios <  2 * avgCeros) {
//					avgEspacios += espacios;
//					j++;
//				}
//			}
//		}
//		avgEspacios/=j;
//
////		double ESPACIO_ENTRE_LETRAS=(avgEspacios+colAvg)/2, ESPACIO_ENTRE_PUNTOS=ESPACIO_ENTRE_LETRAS*0.7;
//
//		ESPACIO_ENTRE_LETRAS = (ESPACIO_ENTRE_LETRAS+avgEspacios)/2;
//		ESPACIO_ENTRE_PUNTOS = (ESPACIO_ENTRE_PUNTOS+avgEspacios)/2;

        // Primer caracter Braille = mayúsculas

//		if(histHor[0] == 0 ){
//			ceros = DecodingUtils.countZeros(histHor, inicio);
//			inicio = ceros;
//			cont = 1;
//		}
        for (start = 0; start < histHor.length && histHor[start] != 0; start++) {
        }
        int zeros = DecodingUtils.countZeros(histHor, start);
        start = zeros;

        if (zerosMayus == 0)
            zerosMayus = (int) ESPACIO_ENTRE_LETRAS;

        BinaryBrailleCodes binaryBrailleCodes = new BinaryBrailleCodes(params);

        // Primer caracter Braille = mayúsculas
        if (zeros > tam * 0.9) {

            for (i = start; i < histHor.length && histHor[i] != 0; i++) {
            }
            int index = i;
            zeros = DecodingUtils.countZeros(histHor, i);
            if (zeros > zerosMayus * 0.9 && zeros < tam * 1.6 && binaryBrailleCodes.processHalfSymbol(start, index, image.getHeight()).equals("101")) {
                cont = 1;
                int leftLine = start - 2 * tam;

                if (leftLine < 0)
                    leftLine = 1;

                salida.getGraphics().drawLine(leftLine, 0, leftLine, image.getHeight());
                params.getSymbolVertLines().add(leftLine);

                zerosMayus = (zerosMayus + zeros) / 2;
            } else {
                salida.getGraphics().drawLine((start - 1), 0, (start - 1), image.getHeight());
                params.getSymbolVertLines().add(start - 1);
            }
        } else {
            salida.getGraphics().drawLine((start - 1), 0, (start - 1), image.getHeight());
            params.getSymbolVertLines().add(start - 1);
        }

        // Procesamos la imagen de izquierda a derecha
        // Cada dos valores igual a cero que encontremos habremos encontrado un caracter Braille (cada uno tiene 2 columnas)
        // Para ir sabiendo los valores que vamos contando, usamos la varible cont

        for (i = start; i < histHor.length; i++) {

            size++;

            // Si encontramos un valor igual a cero

            if (histHor[i] == 0) {
                ceros = DecodingUtils.countZeros(histHor, i);
//				ceros = (int) ((ceros + colDiff.get(cont))/2);
                i = i + ceros - 1;
                cont++;

                cant++;
//
                sizeBefore = size;
                size = (sizeAccum + size) / cant;
//				ESPACIO_ENTRE_LETRAS=1.2*size; ESPACIO_ENTRE_PUNTOS=0.6*size;

                // Si cont no es par
                //lado izquierdo
                if (cont != 0 && cont % 2 != 0 && i != histHor.length - 1) {

                    // Parte derecha del caracter Braille sin puntos + espacio + espacio
                    // Pintamos las líneas correspondientes y las añadimos en la lista de líneas verticales

//					if(ceros >= 3*ESPACIO_ENTRE_LETRAS+3*ESPACIO_ENTRE_PUNTOS+5*size){
////					if(ceros > 6 * tam){
//						//System.out.println(cont+" SIN PUNTO - ESPACIO - ESPACIO");
//						salida.getGraphics().drawLine((int) (i+1-ceros+size+ESPACIO_ENTRE_PUNTOS),0, (int) (i+1-ceros+size+ESPACIO_ENTRE_PUNTOS), image.getHeight());
//						params.getSymbolVertLines().add((int) (i+1-ceros+size+ESPACIO_ENTRE_PUNTOS));
//						cont++;
//						//espacio
//						salida.getGraphics().drawLine((int) (i+4-ceros+size+ESPACIO_ENTRE_PUNTOS),0, (int) (i+4-ceros+size+ESPACIO_ENTRE_PUNTOS), image.getHeight());
//						params.getSymbolVertLines().add((int) (i+4-ceros+size+ESPACIO_ENTRE_PUNTOS));
//
//						cont = cont + checkMayus(histHor,i,ESPACIO_ENTRE_PUNTOS,size, true);
//
//					}

                    // Parte derecha del caracter Braille sin puntos + espacio + mayúsculas
                    // Pintamos las líneas correspondientes y las añadimos en la lista de líneas verticales

//					else if(ceros >= (2*ESPACIO_ENTRE_LETRAS+3*ESPACIO_ENTRE_PUNTOS+4*size)*1.16){
////					if(ceros > 6 * tam){
//						//System.out.println(cont+" SIN PUNTO - ESPACIO - SIN PUNTO");
//						salida.getGraphics().drawLine((int) (i+1-ceros+size+ESPACIO_ENTRE_PUNTOS),0, (int) (i+1-ceros+size+ESPACIO_ENTRE_PUNTOS), image.getHeight());
//						params.getSymbolVertLines().add((int) (i+1-ceros+size+ESPACIO_ENTRE_PUNTOS));
//						cont++;
//
//						//espacio
//						salida.getGraphics().drawLine((int) (i+4-ceros+size+ESPACIO_ENTRE_PUNTOS),0, (int) (i+4-ceros+size+ESPACIO_ENTRE_PUNTOS), image.getHeight());
//						params.getSymbolVertLines().add((int) (i+4-ceros+size+ESPACIO_ENTRE_PUNTOS));
//
//						cont = cont + checkMayus(histHor,i,ESPACIO_ENTRE_PUNTOS,size, true);
//					}

                    // Parte derecha del caracter Braille sin puntos + espacio
                    // Pintamos las líneas correspondientes y las añadimos en la lista de líneas verticales

                    if (ceros >= (2 * ESPACIO_ENTRE_LETRAS + 2 * ESPACIO_ENTRE_PUNTOS + 3 * size) * 0.91) {
//					else if(ceros > 5*tam) {
                        //System.out.println(cont+" SIN PUNTO - ESPACIO");
                        salida.getGraphics().drawLine((int) (i + 1 - ceros + size + ESPACIO_ENTRE_PUNTOS), 0, (int) (i + 1 - ceros + size + ESPACIO_ENTRE_PUNTOS), image.getHeight());
                        params.getSymbolVertLines().add((int) (i + 1 - ceros + size + ESPACIO_ENTRE_PUNTOS));
                        cont++;
                        salida.getGraphics().drawLine((int) (i + 4 - ceros + size + ESPACIO_ENTRE_PUNTOS), 0, (int) (i + 4 - ceros + size + ESPACIO_ENTRE_PUNTOS), image.getHeight());
                        params.getSymbolVertLines().add((int) (i + 4 - ceros + size + ESPACIO_ENTRE_PUNTOS));

//						salida.getGraphics().drawLine(i - 1, 0, i - 1, image.getHeight());
//						params.getSymbolVertLines().add(i - 1);
//						//espacio
//						salida.getGraphics().drawLine(i - 4, 0, i - 4, image.getHeight());
//						params.getSymbolVertLines().add(i - 4);


                        cont = cont + checkMayus(histHor, i, ESPACIO_ENTRE_PUNTOS, size, true);

                    }

                    // Parte derecha del caracter Braille sin puntos + mayúsculas
                    // Pintamos las líneas correspondientes y las añadimos en la lista de líneas verticales

                    else if (ceros >= (ESPACIO_ENTRE_LETRAS + 2 * ESPACIO_ENTRE_PUNTOS + 2 * size) * 1.1) {
//					else if(ceros > 4*tam){
                        //System.out.println(cont+" SIN PUNTO - SIN PUNTO");
                        salida.getGraphics().drawLine((int) (i + 1 - ceros + size + ESPACIO_ENTRE_PUNTOS), 0, (int) (i + 1 - ceros + size + ESPACIO_ENTRE_PUNTOS), image.getHeight());
                        params.getSymbolVertLines().add((int) (i + 1 - ceros + size + ESPACIO_ENTRE_PUNTOS));
                        cont++;

                        cont = cont + checkMayus(histHor, i, ESPACIO_ENTRE_PUNTOS, size, false);

                    }

                    // Parte derecha del caracter Braille sin puntos
                    // Pintamos las líneas correspondientes y las añadimos en la lista de líneas verticales

                    else if (ceros >= (ESPACIO_ENTRE_LETRAS + ESPACIO_ENTRE_PUNTOS + size) * 0.7) {
//					else if(ceros > 2* tam){
                        //System.out.println(cont+" SIN PUNTO");
                        salida.getGraphics().drawLine((int) (i + 1 - ceros + size + ESPACIO_ENTRE_PUNTOS), 0, (int) (i + 1 - ceros + size + ESPACIO_ENTRE_PUNTOS), image.getHeight());
                        params.getSymbolVertLines().add((int) (i + 1 - ceros + size + ESPACIO_ENTRE_PUNTOS));
                        cont++;

//						cont = cont + checkMayus(histHor,i,ESPACIO_ENTRE_PUNTOS,size, false);

                        salida.getGraphics().drawLine(i - 1, 0, i - 1, image.getHeight());
                        params.getSymbolVertLines().add(i - 1);

                    }

                    // Si cont es par
                    // lado derecho
                } else if (cont != 0 && cont % 2 == 0 && i != histHor.length - 1) {
                    // Espacio + Espacio
//					// Pintamos las líneas correspondientes y las añadimos en la lista de líneas verticales
//					if(ceros >= 3*ESPACIO_ENTRE_LETRAS+2*ESPACIO_ENTRE_PUNTOS+4*size) {
//						salida.getGraphics().drawLine(i + 1 - ceros, 0, i + 1 - ceros, image.getHeight());
//						params.getSymbolVertLines().add(i + 1 - ceros);
//						//espacio
//						salida.getGraphics().drawLine(i + 4 - ceros, 0, i + 4 - ceros, image.getHeight());
//						params.getSymbolVertLines().add(i + 4 - ceros);
//
//						cont = cont + checkMayus(histHor,i,ESPACIO_ENTRE_PUNTOS,size, true);
//
//					}
                    // Espacio + mayúsculas
//					// Pintamos las líneas correspondientes y las añadimos en la lista de líneas verticales
//
//					else if(ceros >= (2*ESPACIO_ENTRE_LETRAS+2*ESPACIO_ENTRE_PUNTOS+3*size)*1.11) {
////					if(ceros > 8*tam) {
//						//System.out.println(cont+" ESPACIO - SIN PUNTO");
//						salida.getGraphics().drawLine(i + 1 - ceros, 0, i + 1 - ceros, image.getHeight());
//						params.getSymbolVertLines().add(i + 1 - ceros);
//						salida.getGraphics().drawLine(i + 4 - ceros, 0, i + 4 - ceros, image.getHeight());
//						params.getSymbolVertLines().add(i + 4 - ceros);
//
//						cont = cont + checkMayus(histHor,i,ESPACIO_ENTRE_PUNTOS,size, true);
//
//					}

                    // Espacio
                    // Pintamos las líneas correspondientes y las añadimos en la lista de líneas verticales

                    if (ceros >= (2 * ESPACIO_ENTRE_LETRAS + ESPACIO_ENTRE_PUNTOS + 2 * size) * 0.8) {
//					else if (ceros > 3*tam) {
                        //System.out.println(cont+" ESPACIO");
                        salida.getGraphics().drawLine(i + 1 - ceros, 0, i + 1 - ceros, image.getHeight());
                        params.getSymbolVertLines().add(i + 1 - ceros);
                        //espacio
                        salida.getGraphics().drawLine(i + 4 - ceros, 0, i + 4 - ceros, image.getHeight());
                        params.getSymbolVertLines().add(i + 4 - ceros);

                        cont = cont + checkMayus(histHor, i, ESPACIO_ENTRE_PUNTOS, size, true);

                    }

                    // Mayúsculas dentro de palabra
                    // Pintamos las líneas correspondientes y las añadimos en la lista de líneas verticales

                    else if (ceros >= (ESPACIO_ENTRE_LETRAS + ESPACIO_ENTRE_PUNTOS + size) * 0.9) {
//					else if (ceros >= 2*tam) {
                        //System.out.println(cont+" SIN PUNTO");
                        salida.getGraphics().drawLine(i + 1 - ceros, 0, i + 1 - ceros, image.getHeight());
                        params.getSymbolVertLines().add(i + 1 - ceros);
                        cont = cont + checkMayus(histHor, i, ESPACIO_ENTRE_PUNTOS, size, false);
                    }

                    // Pintamos las líneas correspondientes y las añadimos en la lista de líneas verticales

                    else {//(ceros >= ESPACIO_ENTRE_PUNTOS) {
                        //System.out.println(cont+" HAY PUNTOS");
                        salida.getGraphics().drawLine(i + 1 - ceros, 0, i + 1 - ceros, image.getHeight());
                        params.getSymbolVertLines().add(i + 1 - ceros);

//						cont = cont + checkMayus(histHor,i,ESPACIO_ENTRE_PUNTOS,size, false);

                        salida.getGraphics().drawLine(i - 1, 0, i - 1, image.getHeight());
                        params.getSymbolVertLines().add(i - 1);
                    }
                } else {
                    //System.out.println(cont+" TERMINO");
                    if (cont != 0 && cont % 2 == 0) { //&& ceros > size) {
                        //System.out.println(cont+" FIN");
                        salida.getGraphics().drawLine(i - ceros + 1, 0, i - ceros + 1, image.getHeight());
                        params.getSymbolVertLines().add(i - ceros + 1);
                    } else if (cont != 0 && cont % 2 != 0) {
                        salida.getGraphics().drawLine(i - ceros + 2 * size, 0, i - ceros + 2 * size, image.getHeight());
                        params.getSymbolVertLines().add(i - ceros + 2 * size);
                    }
                }
                sizeAccum += sizeBefore;
                size = 0;
            }
        }
    }

    private int checkMayus(int[] histHor, int i, double ESPACIO_ENTRE_PUNTOS, int size, boolean hasSpaces) {

        int k;
        for (k = i + 1; k < histHor.length && histHor[k] != 0; k++) {
        }
        int zeros = DecodingUtils.countZeros(histHor, k);

        BinaryBrailleCodes binaryBrailleCodes = new BinaryBrailleCodes(params);

        if (zeros > zerosMayus * 0.9 && zeros < size * 1.6 && binaryBrailleCodes.processHalfSymbol(i, k, image.getHeight()).equals("101")) {
            //hay mayuscula
            image.getGraphics().drawLine((int) (i - 1 - ESPACIO_ENTRE_PUNTOS - size), 0, (int) (i - 1 - ESPACIO_ENTRE_PUNTOS - size), image.getHeight());
            params.getSymbolVertLines().add((int) (i - 1 - ESPACIO_ENTRE_PUNTOS - size));
            zerosMayus = (zerosMayus + zeros) / 2;

            if (hasSpaces) {
                image.getGraphics().drawLine((int) (i - 4 - ESPACIO_ENTRE_PUNTOS - size), 0, (int) (i - 4 - ESPACIO_ENTRE_PUNTOS - size), image.getHeight());
                params.getSymbolVertLines().add((int) (i - 4 - ESPACIO_ENTRE_PUNTOS - size));
            }

            return 1;

        } else {
            //no hay mayuscula
            image.getGraphics().drawLine((i - 1), 0, (i - 1), image.getHeight());
            params.getSymbolVertLines().add(i - 1);

            if (hasSpaces) {
                image.getGraphics().drawLine((i - 4), 0, (i - 4), image.getHeight());
                params.getSymbolVertLines().add(i - 4);
            }

            return 0;
        }

    }


    public void _divideSymbols() {

        BufferedImage output = image;

        int[] histHor = horizontalHistogram();

        int size = 0;
        int sizeAccum = tam;
        int sizeBefore = 0;
        int cont = 0;
        int cant = 1;
        int index = 0;
        int zeros = 0;
        if (histHor[0] == 0) {
            zeros = DecodingUtils.countZeros(histHor, index);
            index = zeros;
            cont = 1;
        }

        for (index = 0; histHor[index] != 0 && index < histHor.length; index++) {
        }
        zeros = DecodingUtils.countZeros(histHor, index);
        index = zeros;

        for (int i = index; i < histHor.length; i++) {
            size++;
            if (histHor[i] == 0) {
                zeros = DecodingUtils.countZeros(histHor, i);
                i = i + zeros - 1;
                cont++;
                cant++;

                sizeBefore = size;
                size = (sizeAccum + size) / cant;
                if ((cont != 0) && (cont % 2 != 0) && (i != histHor.length - 1)) {
                    if (zeros > 8 * size) {
                        output.getGraphics().drawLine(i - zeros + size + 4, 0, i - zeros + size + 4, image.getHeight());
                        params.getSymbolVertLines().add(i - zeros + size + 4);
                        cont++;
                        output.getGraphics().drawLine(i - size - 2, 0, i - size - 2, image.getHeight());
                        params.getSymbolVertLines().add(i - size - 2);
                        cont++;
                    } else if (zeros > 5 * size) {
                        output.getGraphics().drawLine(i, 0, i, image.getHeight());
                        params.getSymbolVertLines().add(i);
                        cont++;
                        output.getGraphics().drawLine(i - zeros + size + 4, 0, i - zeros + size + 4, image.getHeight());
                        params.getSymbolVertLines().add(i - zeros + size + 4);
                        cont++;
                        output.getGraphics().drawLine(i - 4, 0, i - 4, image.getHeight());
                        cont++;
                    } else if (zeros > 4 * size) {
                        output.getGraphics().drawLine(i - zeros + 7, 0, i - zeros + 7, image.getHeight());
                        params.getSymbolVertLines().add(i - zeros + size + 4);
                        cont++;
                        output.getGraphics().drawLine(i - zeros + size + 7, 0, i - zeros + size + 7, image.getHeight());
                        params.getSymbolVertLines().add(i - zeros + size + 7);
                        cont++;
                    } else if (zeros >= size * 2) {
                        output.getGraphics().drawLine(i + 1 - zeros + size, 0, i + 1 - zeros + size, image.getHeight());
                        params.getSymbolVertLines().add(i + 1 - zeros + size);
                        output.getGraphics().drawLine(i, 0, i, image.getHeight());
                        params.getSymbolVertLines().add(i);
                        cont++;
                    }
                } else if ((cont != 0) && (cont % 2 == 0) && (i != histHor.length - 1)) {
                    if (zeros > 6 * size) {
                        output.getGraphics().drawLine(i + 1 - zeros, 0, i + 1 - zeros, image.getHeight());
                        params.getSymbolVertLines().add(i + 1 - zeros);
                        output.getGraphics().drawLine(i + 4 - zeros, 0, i + 4 - zeros, image.getHeight());
                        output.getGraphics().drawLine(i - size, 0, i - size, image.getHeight());
                        params.getSymbolVertLines().add(i - size);
                        cont++;
                    } else if (zeros > 3 * size) {
                        output.getGraphics().drawLine(i + 1 - zeros, 0, i + 1 - zeros, image.getHeight());
                        params.getSymbolVertLines().add(i + 1 - zeros);
                        output.getGraphics().drawLine(i, 0, i, image.getHeight());
                        params.getSymbolVertLines().add(i);
                    } else if (zeros > 2 * size) {
                        output.getGraphics().drawLine(i + 1 - zeros, 0, i + 1 - zeros, image.getHeight());
                        params.getSymbolVertLines().add(i + 1 - zeros);
                        output.getGraphics().drawLine(i + 4 - zeros, 0, i + 4 - zeros, image.getHeight());
                        params.getSymbolVertLines().add(i + 4 - zeros);
                        cont++;
                    } else {
                        output.getGraphics().drawLine(i + 1 - zeros, 0, i + 1 - zeros, image.getHeight());
                        params.getSymbolVertLines().add(i + 1 - zeros);
                        output.getGraphics().drawLine(i, 0, i, image.getHeight());
                        params.getSymbolVertLines().add(i);
                    }
                } else if ((cont != 0) && (cont % 2 == 0) && (zeros > size)) {
                    output.getGraphics().drawLine(i - zeros, 0, i - zeros, image.getHeight());
                    params.getSymbolVertLines().add(i - zeros);
                }
                sizeAccum += sizeBefore;
                size = 0;
            }
        }
    }


    private void splitImageCols() {

        BufferedImage auxImage;
        BufferedImage space;
        int width;

        int[] vert = new int[params.getSymbolVertLines().size() + 1];

//		vert[0] = 0;
        vert[0] = image.getWidth() - 1;

        for (int i = 0; i < params.getSymbolVertLines().size(); i++) {
            vert[i + 1] = params.getSymbolVertLines().get(i);
        }


        // Ordenamos el vector

        Arrays.sort(vert);

//		int vertSize=0, count=0;
//
//		for(int i = 0; i < vert.length-1 ; i+=2){
//			vertSize = vertSize + (vert[i+1] - vert[i]);
//			count++;
//		}
//
//		if (count > 1)
//			vertSize = vertSize/count;

        // Recorremos dicho vector de líneas verticales

        for (int i = 0; i < vert.length - 2; i++) {

            // Calculamos el rango para cada dos líneas verticales

            width = vert[i + 1] - vert[i];

            // se splitea la imagen en cada columna del Braille

            auxImage = image.getSubimage(vert[i], 0, width, image.getHeight());
            params.getImageCols().add(auxImage);

            // Espacio
//
//			if(i < vert.length - 2 && (vert[i + 2] - vert[i + 1]) > vertSize*0.8 ){
//				space = image.getSubimage(vert[i + 1],0, (int) (vertSize*0.5), image.getHeight());
//				params.getImageCols().add(space);
//			}

            i++;
        }
    }

    // Metodo que genera el histograma horizontal de la imagen
    // Indica el numero de pixeles blancos de una determinada columna de la imagen

    public int[] horizontalHistogram() {
        int[] hist = new int[image.getWidth()];

        for (int i = 0; i < hist.length; i++) {
            hist[i] = 0;
        }

        // Recorremos la imagen de izquierda a derecha

        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {

                // Si encontramos un píxel blanco aumentamos el valor del histograma en ese índice

                if (image.getRGB(i, j) == -1) {
                    hist[i] = hist[i] + 1;
                }
            }
//			hist[i] = hist[i]-params.getTotalHorLines();
        }
        hist[0] = 0;
        hist[image.getWidth() - 1] = 0;

        // Si el valor es menor de diez, consideramos que no hay píxeles blancos en dicha fila y ponemos el valor del histograma a cero

//		for(int i = 0; i < hist.length ; i++){
//
////			if(hist[i] < image.getHeight()*0.08){
//			if(hist[i] < 3){
//				hist[i] = 0;
//			}
//		}

//		for(int i = 0; i < hist.length-1 ; i++){
//
//			if(i!=0 && hist[i] == 0 && hist[i-1]!=0 && hist[i+1]!=0){
//				hist[i] = 1;
//			}
//		}

        int max = 0;
        boolean flag = false;
        for (int i = 0; i < hist.length; i++) {

            if (hist[i] < 3)
                hist[i] = 0;

            if (i + 1 < hist.length) {
                if (hist[i] > hist[i + 1] && !flag) {
                    max = hist[i];
                    flag = true;
                }

                if (hist[i] < hist[i + 1]) {
                    flag = false;
                    max = 0;
                }
                if (hist[i] < 0.1 * max && flag) {
                    //			if(hist[i] < 10){
                    //			if(hist[i] < image.getWidth()*0.06){
                    hist[i] = 0;
                }
            }
        }

//		JFrame frame = new JFrame("Test");
//		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		frame.setLayout(new BorderLayout());
//		frame.add(new JScrollPane(new Graph(hist,"horizontal")));
//		frame.pack();
//		frame.setLocationRelativeTo(null);
//		frame.setVisible(true);
//		new Graph(hist,"horizontal");

        return hist;
    }
}
