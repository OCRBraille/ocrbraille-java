package Reconocimiento;

import javax.swing.*;
import java.awt.image.BufferedImage;

/**
 * Clase en la que implementamos métodos comunes
 */

public class DecodingUtils {

    // Método que cuenta el número de ceros seguidos de un determinado histograma

    public static int countZeros(int[] hist, int index) {
        int zeros = 0;
        boolean find = false;

        while (index < hist.length && !find) {
            if (hist[index] == 0) {
                zeros++;
            } else {
                find = true;
            }
            index++;
        }

        return zeros;
    }

    // Método que calcula el tamaño aproximado de los círculos blancos

    public static int calculaTam(int[] hist, int indice) {

        int tam = 0;
        boolean enc = false;
        int i = indice;
        int j = 0;

        // Buscamos el primer valor distinto de cero

        while (i < hist.length && !enc) {
            if (hist[i] == 0) {
                i++;
            } else {
                j = i;
                enc = true;
            }
        }

        enc = false;

        // A partir del primer valor encontrado, vamos incremetando el tamaño hasta que encontramos otro valor igual a cero

        while (j < hist.length && !enc) {
            if (hist[j] == 0) {
                enc = true;
            }
            tam++;
            j++;
        }

        return tam + 1;
    }

    // Metodo que genera el histograma vertical de la image
    // Indica el numero de píxeles blancos de una determinada fila de la image

    public static int[] verticalHistogram(boolean divideHist, BufferedImage image) {

        int[] hist = new int[image.getHeight()];

        for (int i = 0; i < hist.length; i++) {
            hist[i] = 0;
        }

        // Recorremos la image de arriba hacia abajo


        for (int j = 0; j < image.getHeight(); j++) {
            for (int i = 0; i < image.getWidth(); i++) {

                // Si encontramos un píxel blanco aumentamos el valor del histograma en ese índice

                if (image.getRGB(i, j) == -1) {
                    hist[j] = hist[j] + 1;
                }
            }
        }
        hist[0] = 0;
        hist[image.getHeight() - 1] = 0;
        // Si el valor de una determinada fila es menor de diez, consideramos que no hay píxeles blancos en dicha fila y ponemos el valor del histograma a cero
        int max = 0;
        boolean flag = false;

        if (divideHist) {
            for (int i = 0; i < hist.length; i++) {

                if (hist[i] < 3)
                    hist[i] = 0;

                if (i + 1 < hist.length) {
                    if (hist[i] > hist[i + 1] && !flag) {
                        max = hist[i];
                        flag = true;
                    }

                    if (hist[i] < hist[i + 1]) {
                        flag = false;
                        max = 0;
                    }
                    if (hist[i] < 0.63 * max && flag) {
                        //			if(hist[i] < 10){
                        //			if(hist[i] < image.getWidth()*0.06){
                        hist[i] = 0;
                    }
                }
            }
//			new Graph(hist,"vertical");
        } else {

            for (int i = 0; i < hist.length; i++) {

                if (hist[i] < 3)
                    hist[i] = 0;
            }
        }
//		JFrame frame = new JFrame("Test");
//		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		frame.setLayout(new BorderLayout());
//		frame.add(new JScrollPane(new Graph(hist,"vertical")));
//		frame.pack();
//		frame.setLocationRelativeTo(null);
//		frame.setVisible(true);
//
//		new Graph(hist,"vertical");

        return hist;
    }

}
