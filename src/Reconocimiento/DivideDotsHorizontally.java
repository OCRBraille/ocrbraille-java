package Reconocimiento;

import java.awt.image.BufferedImage;
import java.util.Arrays;

import Config.InitialParameters;
import Preprocesado.UI;
import org.omg.PortableInterceptor.INACTIVE;

/**
 * Clase en la que implementamos el listener que se encarga de dividir los puntos horizontalmente
 * cada uno de los símbolos Braille.
 */

public class DivideDotsHorizontally {

    private InitialParameters params;
    private BufferedImage image;

    public DivideDotsHorizontally(InitialParameters params) {
        this.params = params;
    }

    // Método que se encarga de Dividir Horizontalmente

    public void doAction() {

        image = params.getImage();

        divideDots();

//		new UI(params).paint(image);
    }

    // Método que se encarga de dividir la image mediante líneas horizontales, delimitando las 3 filas de cada carácter Braille

    public void divideDots() {

        // Creamos una copia de la image

        BufferedImage output = image;

        // Creamos un vector hor con el valor 0, el valor de la altura de la image y todas las líneas horizontales que hemos ido añadiendo en la segmentación

        int[] hor = new int[params.getSymbolHorLines().size()];
//		hor[0] = 0;
//		hor[1] = output.getHeight()-1;
        for (int i = 0; i < params.getSymbolHorLines().size(); i++) {
            hor[i] = params.getSymbolHorLines().get(i);
        }

        // Ordenamos el vector

        Arrays.sort(hor);

        int line1, line2;

        // Recorremos dicho vector de líneas horizontales

        for (int i = 0; i < hor.length - 1; i++) {

            // Calculamos el rango para cada dos líneas horizontales

            int range = hor[i + 1] - hor[i];

            line1 = checkLine(hor[i] + range / 3, hor[i], hor[i + 1]);//, hor[i] + range/3, hor[i+1] ,hor[i],2,1,1);
            line2 = checkLine(hor[i] + 2 * (range / 3), hor[i], hor[i + 1]);//, hor[i] + 2*(range/3),hor[i+1] ,hor[i],1,1, 1);

            // Pintamos las dos líneas correspondientes (para dividir el carácter Braille en 3 filas) y las añadimos en la lista de líneas verticalesBraille

            output.getGraphics().drawLine(0, line1, image.getWidth(), line1);
            params.getDotHorLines().add(line1);
            output.getGraphics().drawLine(0, line2, image.getWidth(), line2);
            params.getDotHorLines().add(line2);
            i++;
        }
    }

    private int checkLine(int y, int up, int down) {//, int original, int limitInf, int limitSup , int inter, int incr, int decr) {

        int[] histVert = DecodingUtils.verticalHistogram(true, image);

        int width = image.getWidth(), newY, i, j;
        boolean encArriba = false;
        boolean encAbajo = false;

//		if (y == limitInf || y == limitSup || y > original || y < original*2)
        if (histVert[y] == 0)
            return y;

        for (i = y; i < down - 1 && !encAbajo; i++) {
            if (histVert[i] == 0)
                encAbajo = true;
        }

        for (j = y; j > up && !encArriba; j--) {
            if (histVert[j] == 0)
                encArriba = true;
        }

        if (encArriba && encAbajo)
            if (i - y < y - j)
                return i;
            else
                return j;

        if (encAbajo)
            return i;

        if (encArriba)
            return j;


        return y;

//		if (enc)
//			if (inter % 2 == 0)
//				newY = checkLine(original-decr, original, limitInf, limitSup, ++inter, incr, ++decr);
//			else
//				newY = checkLine(original+incr, original, limitInf, limitSup, ++inter, ++incr, decr);
//		else
//			newY = y;
//
//		return newY;
    }

}