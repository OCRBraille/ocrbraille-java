package Reconocimiento;

import java.util.ArrayList;
import java.util.HashMap;

import Config.InitialParameters;
import Preprocesado.UI;

/**
 * Clase en la que implementamos el listener que se encarga de decodificar el mensaje.
 */

public class Decode {

    private InitialParameters params;
    private static boolean num;
    private static boolean may;
    private static boolean intro;
    private static final HashMap<String, String> alfabeto = createMap(); // HashMap donde almacenamos el alfabeto.

    private static HashMap<String, String> createMap() {

        HashMap<String, String> myMap = new HashMap<>();

        //http://es.wikipedia.org/wiki/Braille_espa%C3%B1ol

        // Creamos una estructura de datos (HashMap) donde almacenamos el alfabeto Braille
        // Cada clave es el código binario de un símbolo Braille y el valor es el caráceter en castellano

        myMap.put("000000", " ");
        myMap.put("100000", "a 1");
        myMap.put("110000", "b 2");
        myMap.put("100100", "c 3");
        myMap.put("100110", "d 4");
        myMap.put("100010", "e 5");
        myMap.put("110100", "f 6");
        myMap.put("110110", "g 7");
        myMap.put("110010", "h 8");
        myMap.put("010100", "i 9");
        myMap.put("010110", "j 0");
        myMap.put("101000", "k");
        myMap.put("111000", "l");
        myMap.put("101100", "m");
        myMap.put("101110", "n");
        myMap.put("110111", "ñ");
        myMap.put("101010", "o >"); //TODO: Problema con el >
        myMap.put("111100", "p");
        myMap.put("111110", "q");
        myMap.put("111010", "r");
        myMap.put("011100", "s");
        myMap.put("011110", "t");
        myMap.put("101001", "u");
        myMap.put("111001", "v");
        myMap.put("010111", "w");
        myMap.put("101101", "x");
        myMap.put("101111", "y");
        myMap.put("101011", "z");
        myMap.put("111011", "á [");    //TODO: problema con el "["
        myMap.put("011101", "é");
        myMap.put("001100", "í");
        myMap.put("001101", "ó");
        myMap.put("011111", "ú ]");
        myMap.put("110011", "ü");
        myMap.put("111101", "&");
        myMap.put("001000", ".");
        myMap.put("001111", "num");
        myMap.put("000101", "may");
        myMap.put("010000", ",");
        myMap.put("010001", "?"); //TODO: representar "¿?" (usa el mismo signo)
        myMap.put("011000", ";");
        myMap.put("010010", ":");
        myMap.put("011010", "! +"); //TODO: representar "¡!" (usa el mismo signo) y problema con el "+"
        myMap.put("011001", "\" x"); //TODO: problema con el "x"
        myMap.put("110001", "(");
        myMap.put("001110", ")");
        myMap.put("001001", "-");
        myMap.put("001010", "*");
        myMap.put("011011", "=");
        myMap.put("010101", "<");
        myMap.put("010011", "÷");
        myMap.put("001011", "º");    //grados
        myMap.put("000111", "|");    //TODO: ver como resolver "{" y "}"
        myMap.put("111111", "intro");

        return myMap;
    }

    public Decode(InitialParameters params) {
        this.params = params;
    }

    // Metodo que se encarga de decodificar el mensaje

    public String doAction() {

        String message = decodeMessage();

        //para imprimir cada letra
//		new UI(params).paint(params.getImage(), message);
        return message;
    }

    // Metodo que realiza la decodificacion del mensaje en codigo Braille, devuelve el texto decodificado al castellano

    public String decodeMessage() {

        StringBuilder mensaje = new StringBuilder("");
        ArrayList<String> codes = params.getBrailleCodes();
        int i = 0;

        // Recorremos el vector patron

        while (codes.size() != 0 && i < codes.size()) {

            // Traducimos un renglón y lo añadimos a la traducción

            String m = decodeSymbol(codes.get(i));

            mensaje.append(m);
            i++;
        }

//		// añadimos un salto de línea al final del renglón
//		mensaje.append("\n");

//		return mensaje.toString().trim();
        return mensaje.toString();
    }

    // Método que se encarga de decodificar un simbolo del mensaje en código Braille

    public String decodeSymbol(String binaryCode) {

        String trans = "";
        String symbol = alfabeto.get(binaryCode);

        if (symbol == null) {
            String rightBinary = binaryCode.substring(3, 6);
            if (rightBinary.equals("101"))// || rightBinary.equals("111"))
                symbol = "may";
            else
                symbol = "☺";
        }

        if (symbol.equals("num")) {
            num = true;
        }
        if (symbol.equals("may")) {
            may = true;
        }
        if (symbol.equals("intro")) {
            intro = true;
        }

        // Si hemos procesado el carácter que indica número

        if (num) {
            if (!symbol.equals("num") && (symbol.length() > 2)) {

                // Tomamos el caráceter numérico almacenado en el valor (posición 2)
                trans = trans + symbol.charAt(2);
                num = false;
            }

            // Si hemos procesado el carácter que indica mayúculas

        } else if (may) {
            if (!symbol.equals("may")) {

                // Traducimos en mayúsculas

                trans = trans + symbol.toUpperCase().charAt(0);
                may = false;
            }

            // Si hemos procesado el carácter que indica intro introducimos un salto de línea

        } else if (intro) {
            trans = trans + "\n";
            intro = false;
        } else {
            trans = trans + symbol.charAt(0);
        }

        return trans;
    }

}