package Reconocimiento;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * Clase para imprimir el histograma (vertical y horizontal)
 */

class Graph extends JPanel {

    private static final int MIN_BAR_WIDTH = 4;
    private int[] mapHistory;

    public Graph(int[] mapHistory, String tipo) {
        this.mapHistory = mapHistory;
        int width = (mapHistory.length * MIN_BAR_WIDTH) + 11;
        Dimension minSize = new Dimension(width, 128);
        Dimension prefSize = new Dimension(width, 8000);
        setMinimumSize(minSize);
        setPreferredSize(prefSize);

        setSize(getPreferredSize());
        BufferedImage image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g = image.createGraphics();
        printAll(g);
        g.dispose();

        String timeStamp = new SimpleDateFormat("HH_mm_ss_SSS").format(new java.util.Date());

        try {
            ImageIO.write(image, "png", new File("src/Test_Main/images/histograma_" + tipo + "_" + timeStamp + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (mapHistory != null) {
            int xOffset = 5;
            int yOffset = 5;
            int width = getWidth() - 1 - (xOffset * 2);
            int height = getHeight() - 1 - (yOffset * 2);
            Graphics2D g2d = (Graphics2D) g.create();
            g2d.setColor(Color.DARK_GRAY);
            g2d.drawRect(xOffset, yOffset, width, height);
            int barWidth = Math.max(MIN_BAR_WIDTH,
                    (int) Math.floor((float) width
                            / (float) mapHistory.length));
//            System.out.println("width = " + width + "; size = "
//                    + mapHistory.length + "; barWidth = " + barWidth);
            int maxValue = 0;
            for (int value : mapHistory) {
                maxValue = Math.max(maxValue, value);
            }
            int xPos = xOffset;
            for (int i = 0; i < mapHistory.length; i++) {
                int value = mapHistory[i];
                int barHeight = Math.round(((float) value
                        / (float) maxValue) * height);
                g2d.setColor(new Color(17, 34, 100));
                int yPos = height + yOffset - barHeight;
                //Rectangle bar = new Rectangle(xPos, yPos, barWidth, barHeight);
                Rectangle2D bar = new Rectangle2D.Float(
                        xPos, yPos, barWidth, barHeight);
                g2d.fill(bar);
                g2d.setColor(Color.DARK_GRAY);
                g2d.draw(bar);
                xPos += barWidth;
            }
            g2d.dispose();
        }
    }
}