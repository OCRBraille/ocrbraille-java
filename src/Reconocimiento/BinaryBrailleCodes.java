package Reconocimiento;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;

import Config.InitialParameters;

import static Preprocesado.DetectCircles.tam;

/**
 * Clase en la que implementamos el listener que se encarga de crear el vector patron
 * con el texto en codigo Braille que debemos decodificar.
 */

public class BinaryBrailleCodes {

    private InitialParameters params;
    private BufferedImage image;
    private int circleArea;

    public BinaryBrailleCodes(InitialParameters params) {
        this.params = params;
    }

    // Metodo que se encarga de crear el Vector con los patrones de los puntos

    public void doAction() {

        image = params.getImage();

        circleArea = (int) (((int) Math.pow(tam, 2) / 4) * Math.PI);

        detectDots();

    }

    // Metodo que crea el vector patron. Iremos procesando cada uno de los renglones de izquierda a derecha

    public void detectDots() {

        // Creamos un vector vert con todas las líneas verticales que hemos ido añadiendo en la segmentación

        int[] vert = new int[params.getSymbolVertLines().size() + params.getDotVertLines().size() + 2];
//		vert[0] = 0;
        vert[0] = image.getWidth() - 1;
        for (int i = 0; i < params.getSymbolVertLines().size(); i++) {
            vert[i + 1] = params.getSymbolVertLines().get(i);
        }
        for (int i = 0; i < params.getDotVertLines().size(); i++) {
            vert[params.getSymbolVertLines().size() + 1 + i] = params.getDotVertLines().get(i);
        }

        // Ordenamos el vector

        Arrays.sort(vert);

        // Creamos un vector hor con todas las líneas horizontales que hemos ido añadiendo en la segmentación

        int[] hor = new int[params.getSymbolHorLines().size() + params.getDotHorLines().size()];
//		hor[0] = 0;
//		hor[1] = image.getHeight()-1;
        for (int i = 0; i < params.getSymbolHorLines().size(); i++) {
            hor[i] = params.getSymbolHorLines().get(i);//-params.getSymbolHorLines().get(0);
        }

        for (int i = 0; i < params.getDotHorLines().size(); i++) {
            hor[params.getSymbolHorLines().size() + i] = params.getDotHorLines().get(i);//-params.getSymbolHorLines().get(0);;
        }

        // Ordenamos el vector

        Arrays.sort(hor);

        // Creamos el ArrayList donde almacenamos el vector patron

        ArrayList<String> codes = params.getBrailleCodes();


        int vertSize = 0, count = 0;

        for (int i = 0; i <= vert.length - 3; i += 3) {
            vertSize = vertSize + (vert[i + 2] - vert[i]);
            count++;
        }

        if (count > 1)
            vertSize = vertSize / count;

        String symbol;

        for (int j = 0; j < hor.length - 3; j++) {
            for (int i = 0; i < vert.length - 2; i++) {

                // Vamos procesando los diferentes simbolos

                symbol = processSymbol(vert[i], vert[i + 2], hor[j], hor[j + 3], vert[i + 1], hor[j + 1], hor[j + 2]);
                codes.add(symbol);

                // Espacio

                if (i < vert.length - 4 && (vert[i + 3] - vert[i + 2]) > vertSize) {
                    codes.add("000000");
                }
                i = i + 2;
            }

//			// Intro
//
//			if(j < hor.length - 5 && (hor[j + 4] - hor[j + 3]) > horSize ){
//				codes.add("111111");
//			}
//			codes.add("111111");

            j = j + 3;
        }

    }

    // Método que procesa un determinado símbolo Braille y devuelve una cadena con su código binario para su posterior traducción

    public String processSymbol(int wInicial, int wFinal, int hInicial, int hFinal, int vert, int hor1, int hor2) {
        String symbol = "";

        // Casilla 1

        symbol = symbol + processCell(wInicial, vert, hInicial, hor1);

        // Casilla 2

        symbol = symbol + processCell(wInicial, vert, hor1, hor2);

        // Casilla 3

        symbol = symbol + processCell(wInicial, vert, hor2, hFinal);

        // Casilla 4

        symbol = symbol + processCell(vert, wFinal, hInicial, hor1);

        // Casilla 5

        symbol = symbol + processCell(vert, wFinal, hor1, hor2);

        // Casilla 6

        symbol = symbol + processCell(vert, wFinal, hor2, hFinal);

        return symbol;
    }

    // Método que procesa una determinada casilla de un símbolo Braille. Devuelve un uno o un cero dependiendo del número de píxeles blancos que contenga dicha casilla

    public String processCell(int wInicial, int wFinal, int hInicial, int hFinal) {
        String cell = "";
        //BufferedImage imag = params.getImagenAux();

        int white = 0;

        // Contamos el número de píxeles blancos

        for (int i = wInicial + 1; i < wFinal; i++) {
            for (int j = hInicial + 1; j < hFinal; j++) {
                if (image.getRGB(i, j) == -1) {
                    white++;
                }
            }
        }


        //		if( white > (wFinal-wInicial)*(hFinal-hInicial)*0.26 ){		//TODO JP: revisar el número mas óptimo
//		if( white > (wFinal-wInicial)*(hFinal-hInicial)*0.15){		//TODO JP: revisar el número mas óptimo
        if (white > circleArea * 0.221) {        //TODO JP: revisar el número mas óptimo
            cell = cell + "1";
        } else {
            cell = cell + "0";
        }

        return cell;
    }

    public String processHalfSymbol(int wInicial, int wFinal, int hFinal) {
        String symbol = "";
        image = params.getImage().getSubimage(wInicial, 0, wFinal - wInicial, hFinal);
        circleArea = (int) (((int) Math.pow(tam, 2) / 4) * Math.PI);

        int hInicial = (int) (hFinal * 0.04);
        hFinal = (int) (hFinal * 0.96);

        int hor1 = (hFinal - hInicial) / 3;
        int hor2 = (hFinal - hInicial) * 2 / 3;

//		image.getGraphics().drawLine(0,hor1, image.getWidth(), hor1);
//		image.getGraphics().drawLine(0,hor2, image.getWidth(), hor2);

        // Casilla 1

        symbol = symbol + processCell(0, image.getWidth() - 1, hInicial, hor1);

        // Casilla 2

        symbol = symbol + processCell(0, image.getWidth() - 1, hor1, hor2);

        // Casilla 3

        symbol = symbol + processCell(0, image.getWidth() - 1, hor2, hFinal);

        return symbol;
    }
}