package Reconocimiento;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import Config.InitialParameters;
import Preprocesado.UI;

import javax.swing.*;

/**
 * Clase en la que implementamos el listener que se encarga de dividir los simbolos de la
 * image mediante lineas horizontales, delimitando los diferentes renglones del Braille.
 */

public class DivideSymbolHorizontally {

    private InitialParameters params; // Ventana principal de la aplicación
    private BufferedImage image;

    public DivideSymbolHorizontally(InitialParameters params) {
        this.params = params;
    }

    // Metodo que se encarga de dividir en lineas horizontales

    public void doAction() {

        image = params.getImage();

        divideSymbol();

        splitImageRows();

        new UI(params).paint(image);
    }

    // Metodo que se encarga de dividir la image mediante lineas horizontales
    // delimitando los diferentes renglones con texto en código Braille

    public void divideSymbol() {

        BufferedImage output = image;

        int[] histVert = DecodingUtils.verticalHistogram(true, image);
        //int tam = DecodingUtils.calculaTam(histVert,0);
//		int cont = -1;
        int start = 0;

        //new
        int size = 0;
        int sizeAccum = 0;
        int sizeBefore = 0;
        int cont = 0;
        int cant = 0;
        //new

        // Comenzamos a procesar a partir del primer valor del histograma distinto de cero

        for (start = 0; start < histVert.length && histVert[start] != 0; start++) {
        }
        int zeros = DecodingUtils.countZeros(histVert, start);
        start = zeros + 1;
        // Procesamos la image de arriba hacia abajo
        // Cada tres valores igual a cero que encontremos habremos encontrado un caracter Braille (cada uno tiene 3 filas)
        // Para ir sabiendo los valores que vamos contando, usamos la varible cont

        for (int i = start; i < histVert.length; i++) {
            //new
            size++;
            //new

            // Si encontramos un valor igual a cero

            if (histVert[i] == 0) {
                zeros = DecodingUtils.countZeros(histVert, i);
                i = i + zeros - 1;
                cont++;

                //new
                cant++;
                sizeBefore = size;
                size = (sizeAccum + size) / cant;
                //new

                if (zeros < 2) {
                    cont--;
                }

                // Si cont es par, pero no es múltiplo de tres

                else if (cont != 0 && cont % 3 != 0) {//&& cont % 2 == 0){

                    // Si cabe otro punto (comparamos con tam) es porque los caracteres Braille de la línea, no tienen puntos en blanco en la fila de abajo
                    // Pintamos las líneas correspondientes y las añadimos en la lista de líneas horizontales

                    if (zeros > 2 * size) {
//					if(ceros > 2* tam ){
                        int aux = (i + 1 - zeros + 2 * size);
                        if (aux > image.getHeight()) {
                            aux = image.getHeight() - 1;
                        }
                        output.getGraphics().drawLine(0, aux, image.getWidth(), aux);
                        params.setTotalHorLines(params.getTotalHorLines() + 1);
                        params.getSymbolHorLines().add(aux);
                        output.getGraphics().drawLine(0, i, image.getWidth(), i);
                        params.setTotalHorLines(params.getTotalHorLines() + 1);
                        params.getSymbolHorLines().add(i);

                        if (cont % 3 == 1)
                            cont++;

                        cont++;
                    }

                    // Si cont es multiplo de tres
                    // Pintamos las lineas correspondientes y las añadimos en la lista de líneas horizontales

                } else if (cont != 0 && cont % 3 == 0 && i != histVert.length - 1) {

                    int aux = (i + 1 - zeros + size / 3);
                    if (aux > image.getHeight()) {
                        aux = image.getHeight() - 1;
                    }

                    output.getGraphics().drawLine(0, aux, image.getWidth(), aux);
                    params.setTotalHorLines(params.getTotalHorLines() + 1);
                    params.getSymbolHorLines().add(aux);
                    output.getGraphics().drawLine(0, i, image.getWidth(), i);
                    params.setTotalHorLines(params.getTotalHorLines() + 1);
                    params.getSymbolHorLines().add(i);
                }

                sizeAccum += sizeBefore;
                size = 0;
            }
        }
    }

    public void splitImageRows() {

        BufferedImage auxImage;
        int height;
        int[] hor = new int[params.getSymbolHorLines().size() + 2];

        hor[0] = 0;
        hor[1] = image.getHeight() - 1;

        for (int i = 0; i < params.getSymbolHorLines().size(); i++) {
            hor[i + 2] = params.getSymbolHorLines().get(i);
        }

        Arrays.sort(hor);

        // Recorremos dicho vector de líneas horizontales

        for (int i = 0; i < hor.length - 1; i++) {

            height = hor[i + 1] - hor[i];
            // se splitea la imagen en cada renglon del Braille

            auxImage = image.getSubimage(0, hor[i], image.getWidth(), height);
            params.getImageRows().add(auxImage);

            i++;
        }
//		Collections.reverse(params.getImageRows());
    }

    public void checkSpaces() {

        image = params.getImage();

        checkUpDownSpace();

//		new UI(params).paint(image);

    }

    private void checkUpDownSpace() {
        int limitUp = (int) (image.getHeight() * 0.29), i;
        int limitDown = (int) ((image.getHeight()) * 0.74);
        int height = image.getHeight() - 1;

        int[] histVert = DecodingUtils.verticalHistogram(false, image);

        for (i = 1; i <= limitUp && histVert[i] == 0; i++) ;

        if (i <= limitUp) {
            image.getGraphics().drawLine(0, i - 1, image.getWidth(), i - 1);
            params.setTotalHorLines(params.getTotalHorLines() + 1);
            params.getSymbolHorLines().add(i);
        } else {
            image.getGraphics().drawLine(0, 1, image.getWidth(), 1);
            params.setTotalHorLines(params.getTotalHorLines() + 1);
            params.getSymbolHorLines().add(1);
        }

        for (i = height - 1; i >= limitDown && histVert[i] == 0; i--) ;

        if (i >= limitDown) {
            image.getGraphics().drawLine(0, i - 1, image.getWidth(), i - 1);
            params.setTotalHorLines(params.getTotalHorLines() + 1);
            params.getSymbolHorLines().add(i);
        } else {
            image.getGraphics().drawLine(0, height - 1, image.getWidth(), height - 1);
            params.setTotalHorLines(params.getTotalHorLines() + 1);
            params.getSymbolHorLines().add(height - 1);
        }

    }
}


