package Reconocimiento;

import java.awt.image.BufferedImage;
import java.util.Arrays;

import Config.InitialParameters;
import Preprocesado.UI;

/**
 * Clase en la que implementamos el listener que se encarga de dividir los puntos verticalmente
 * cada uno de los simbolos Braille.
 */

public class DivideDotsVertically {

    private InitialParameters params; // Ventana principal de la aplicación
    private BufferedImage imagen; // Imagen que se está procesando en la aplicación

    public DivideDotsVertically(InitialParameters params) {
        this.params = params;
    }

    // Metodo que se encarga de Dividir Verticalmente

    public void doAction() {

        imagen = params.getImage();

        divideDots();

//		new UI(params).paint(imagen);
    }


    // Metodo que se encarga de dividir la imagen mediante lineas verticales, delimitando las 2 columnas de cada caracter Braille

    public void divideDots() {

        // Creamos una copia de la imagen

        BufferedImage salida = imagen;

        // Creamos un vector ver con el valor 0, el valor de la anchura de la imagen y todas las líneas verticales que hemos ido añadiendo en la segmentacion

        int[] vert = new int[params.getSymbolVertLines().size() + 2];
        vert[0] = 0;
        vert[0] = salida.getWidth() - 1;
        int i;
        for (i = 0; i < params.getSymbolVertLines().size(); i++) {
            vert[i + 1] = params.getSymbolVertLines().get(i);
        }

        // Ordenamos el vector

        Arrays.sort(vert);

        // Recorremos dicho vector de líneas verticales

        for (i = 0; i < vert.length - 1; i++) {

            // Calculamos el rango para cada dos líneas verticales

            int rango = vert[i + 1] - vert[i];

            // Pintamos la línea correspondientes (para dividir el caracter Braille en 2 columnas) y la añadimos en la lista de líneas horizontalesBraille
            salida.getGraphics().drawLine(vert[i] + rango / 2, 0, vert[i] + rango / 2, imagen.getHeight());
            params.getDotVertLines().add(vert[i] + rango / 2);

            i++;
        }
    }
}