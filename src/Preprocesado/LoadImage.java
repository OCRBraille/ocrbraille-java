package Preprocesado;

import Config.InitialParameters;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

/**
 * Clase en la que implementamos el listener que se encarga de cargar la imagen que vamos a procesar.
 */

public class LoadImage {

    private InitialParameters params;

    public LoadImage(InitialParameters params) {
        this.params = params;
    }

    // Método que se encarga de cargar la imagen

    public void doAction(String path) {

        Mat matImage = Highgui.imread(path, Highgui.CV_LOAD_IMAGE_GRAYSCALE);
        params.setMatImage(matImage);

        new UI(params).paint(null);
    }

}
