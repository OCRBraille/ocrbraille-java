package Preprocesado;

import Config.InitialParameters;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;

/**
 * Clase en la que implementamos el listener que se encarga de dilatar los caracteres Braille.
 */

public class Dilate {

    private InitialParameters params;

    public Dilate(InitialParameters params) {
        this.params = params;
    }

    // Método que se encarga de Dilatar los puntos de la imagen

    public void doAction() {

        dilate();

//		dilate(params.getImage());

        new UI(params).paint(null);
    }


    public void dilate() {
        Mat matImage = params.getMatImage();

        int erosion_size = 1;

        Mat element = Imgproc.getStructuringElement(
                Imgproc.MORPH_ELLIPSE, new Size(2 * erosion_size + 1, 2 * erosion_size + 1),
                new Point(erosion_size, erosion_size)
        );

        Imgproc.erode(matImage, matImage, element);
        //Imgproc.dilate(source, destination,element);
        //Imgproc.morphologyEx(source, destination, Imgproc.MORPH_CLOSE, kernel);
    }

    // Metodos anteriores
    // Método que dilata los carácteres Braille
    // Hemos utilizado el método "filter" de la clase "ConvolveOp" del paquete "java.awt.image"
    @Deprecated
    public void dilate(BufferedImage image) {
        Kernel kernel = new Kernel(3, 3, new float[]{
                1f, 1f, 1f,
                1f, 1f, 1f,
                1f, 1f, 1f
        });
        ConvolveOp op = new ConvolveOp(kernel);
        op.filter(image, image);
    }
}