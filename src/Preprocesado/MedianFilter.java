package Preprocesado;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Arrays;

import Config.InitialParameters;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;


/**
 * Clase en la que implementamos el listener que se encarga de realizar un filtro mediana
 */

public class MedianFilter {

    private InitialParameters params;

    public MedianFilter(InitialParameters params) {
        this.params = params;
    }

    // Método que se encarga de eliminar el ruido de la imagen a traves del filtro mediana

    public void doAction() {

        medianFilter();

//		medianFilter(params.getImage());

        new UI(params).paint(null);
    }

    public void medianFilter() {

        Mat matImage = params.getMatImage();

//		Imgproc.threshold(matImage, matImage, 0, 255, Imgproc.THRESH_OTSU);

//		Imgproc.medianBlur(matImage, matImage, 5);
        Imgproc.medianBlur(matImage, matImage, 7);
//		Imgproc.GaussianBlur(matImage, matImage, new Size(3, 3), 1, 1);

    }

    // Metodo Anterior
    // Metodo que realiza el filtro de la mediana a la imagen
    @Deprecated
    public void medianFilter(BufferedImage image) {

        BufferedImage salida = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());

        // i va recorriendo la anchura
        // j la altura

        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {

                // Para cada píxel de la imagen se calcula la mediana de los valores de su entorno y se asigna dicho valor al píxel

                int med = mediana(image, i, j);
                image.setRGB(i, j, med);
            }
        }
    }

    // Metodo que calcula la mediana de un determinado píxel

    public int mediana(BufferedImage image, int i, int j) {

        int med; // Mediana
        int[] val = new int[9]; // Vector donde almacenamos los valores a los que les calculamos la mediana

        Color c = Color.WHITE;
        int col = c.getRGB();

        // En la posición 0 del vector almacenamos el valor del píxel

        val[0] = image.getRGB(i, j);

        // En las posiciones 1 a 8 vamos almacenando los valores de los píxeles del entorno 3x3
        // Si procesamos un píxel del borde, asignamos el color blanco a las posiciones del entorno que no existen

        if (j + 1 >= image.getHeight()) {
            val[1] = col;
        } else {
            val[1] = image.getRGB(i, j + 1);
        }

        if (i - 1 < 0 || j + 1 >= image.getHeight()) {
            val[2] = col;
        } else {
            val[2] = image.getRGB(i - 1, j + 1);
        }

        if (i - 1 < 0) {
            val[3] = col;
        } else {
            val[3] = image.getRGB(i - 1, j);
        }

        if (i - 1 < 0 || j - 1 < 0) {
            val[4] = col;
        } else {
            val[4] = image.getRGB(i - 1, j - 1);
        }

        if (j - 1 < 0) {
            val[5] = col;
        } else {
            val[5] = image.getRGB(i, j - 1);
        }

        if (i + 1 >= image.getWidth() || j - 1 < 0) {
            val[6] = col;
        } else {
            val[6] = image.getRGB(i + 1, j - 1);
        }

        if (i + 1 >= image.getWidth()) {
            val[7] = col;
        } else {
            val[7] = image.getRGB(i + 1, j);
        }

        if (i + 1 >= image.getWidth() || j + 1 >= image.getHeight()) {
            val[8] = col;
        } else {
            val[8] = image.getRGB(i + 1, j + 1);
        }

        // Ordenamos el vector

        Arrays.sort(val);

        // Obtenemos la mediana de los valores tomando el valor que se encuentra en la mitad del vector

        med = val[4];

        return med;
    }

}
