package Preprocesado;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.imageio.ImageIO;
import javax.swing.*;

import Config.InitialParameters;

/**
 * Clase que se encarga de la UI para mostrar las imagenes intermedias
 */

public class UI {

    private InitialParameters params;

    public UI(InitialParameters params) {
        this.params = params;
    }

    // Método que se encarga de dibujar el resultado de cada uno de los listeners de los pasos
    public void __paint(BufferedImage image) {

        if (image == null)
            image = params.convertImage();

        // Use a label to display the image
        JFrame frame = new JFrame();

        //JP: SI LA IMAGEN SUPERA LA RESOLUCION DE PANTALLA HAY QUE ESCALAR LA IMAGEN

        // ESCALANDO LA IMAGEN
//		Image scaled = image.getScaledInstance(image.getWidth()/2, image.getHeight()/2, Image.SCALE_SMOOTH);
//		JLabel lblimage = new JLabel(new ImageIcon(scaled));

        // SIN ESCALAR LA IMAGEN
        Image imagen = image.getScaledInstance(image.getWidth(), image.getHeight(), Image.SCALE_SMOOTH);
        JLabel lblimage = new JLabel(new ImageIcon(imagen));

        frame.getContentPane().add(lblimage, BorderLayout.CENTER);
        frame.setSize(300, 400);
        frame.setVisible(true);
        frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(lblimage);
        frame.add(mainPanel);
        frame.setVisible(true);
    }

    //Metodo para guardar las imagenes en archivos
    public void _paint(BufferedImage image) {

        if (image == null)
            image = params.convertImage();

        String timeStamp = new SimpleDateFormat("HH_mm_ss_SSS").format(new java.util.Date());

        File outputFile = new File("src/Test_Main/images/imagen " + timeStamp + ".png");

        try {
            ImageIO.write(image, "png", outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Metodo dummy para que no dibuje las imagenes intermedias, renombrar a paint
    public void paint(BufferedImage dummy) {
    }

    public void paint(BufferedImage image, String message) {
        if (image == null)
            image = params.convertImage();

        if (message.equals("|") || message.equals("<") || message.equals(":") || message.equals(" ") || message.equals("/"))
            message = "signo";

        String timeStamp = new SimpleDateFormat("-HH-mm-ss-SSS").format(new java.util.Date());

        File outputFile = new File("src/Test_Main/images/" + message + timeStamp + ".png");

        try {
            ImageIO.write(image, "png", outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Metodo dummy para que no dibuje las imagenes intermedias, renombrar a paint
    public void _paint(BufferedImage image, String message) {
    }

}
