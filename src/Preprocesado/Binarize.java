package Preprocesado;

import java.awt.Color;
import java.awt.image.BufferedImage;

import Config.InitialParameters;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import static org.opencv.imgproc.Imgproc.*;

/**
 * Clase en la que implementamos el listener que se encarga de binarizar la imagen.
 */

public class Binarize {

    private InitialParameters params;

    public Binarize(InitialParameters params) {
        this.params = params;
    }

    // Método que se encarga de Binarizar la imagen con Umbral Adaptativo (adaptiveThreshold)

    public void doAction() {

        binarize();

//		binarize(params.getImage());

        new UI(params).paint(null);

    }

    private void binarize() {

        Mat matImage = params.getMatImage();

        Imgproc.adaptiveThreshold(matImage, matImage, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 139, 4);

        //Imgproc.adaptiveThreshold(source,destination,255,Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY,139, 4);
        //Imgproc.adaptiveThreshold(source,destination,255,Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY,139, 4);
        //Imgproc.threshold(source,destination,0,218,THRESH_BINARY_INV);
        //Imgproc.adaptiveThreshold(source, destination, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 201, 10);
        //Imgproc.adaptiveThreshold(source, destination, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 141, 10);
        //Imgproc.adaptiveThreshold(source,destination,	255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 41, 15);

        //Imgproc.threshold(matImage, matImage, 0, 255, Imgproc.THRESH_OTSU);
    }

    // Return histogram of grayscale image
    private static int[] imageHistogram(BufferedImage input) {

        int[] histogram = new int[256];

        for (int i = 0; i < histogram.length; i++) histogram[i] = 0;

        for (int i = 0; i < input.getWidth(); i++) {
            for (int j = 0; j < input.getHeight(); j++) {
                int red = new Color(input.getRGB(i, j)).getRed();
                histogram[red]++;
            }
        }

        return histogram;

    }

    // Metodos anteriores
    // BINARIZACIÓN DE LA IMAGEN
    // http://zerocool.is-a-geek.net/?p=376%20%28Otsu%29
    // Get binary treshold using Otsu's method
    @Deprecated
    private static int otsuTreshold(BufferedImage image) {

        int[] histogram = imageHistogram(image);
        int total = image.getHeight() * image.getWidth();

        float sum = 0;
        for (int i = 0; i < 256; i++) sum += i * histogram[i];

        float sumB = 0;
        int wB = 0;
        int wF = 0;

        float varMax = 0;
        int threshold = 0;

        for (int i = 0; i < 256; i++) {
            wB += histogram[i];
            if (wB == 0) continue;
            wF = total - wB;

            if (wF == 0) break;

            sumB += (float) (i * histogram[i]);
            float mB = sumB / wB;
            float mF = (sum - sumB) / wF;

            float varBetween = (float) wB * (float) wF * (mB - mF) * (mB - mF);

            if (varBetween > varMax) {
                varMax = varBetween;
                threshold = i;
            }
        }

        return threshold;

    }

    @Deprecated
    private static void binarize(BufferedImage image) {

        int threshold = otsuTreshold(image);

        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {
                int red = new Color(image.getRGB(i, j)).getRed();
                int alpha = new Color(image.getRGB(i, j)).getAlpha();
                int newPixel;
                if (red > threshold) {
                    newPixel = 255;
                } else {
                    newPixel = 0;
                }
                newPixel = ImageUtils.colorToRGB(alpha, newPixel, newPixel, newPixel);
                image.setRGB(i, j, newPixel);
            }
        }
    }


}

