package Preprocesado;

import java.awt.Color;
import java.awt.image.BufferedImage;

import Config.InitialParameters;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 * Clase en la que implementamos el listener que se encarga de realizar la conversion a escala de grises.
 */

public class Greyscale {

    private InitialParameters params;

    public Greyscale(InitialParameters params) {
        this.params = params;
    }

    // Método que se encarga de convertir la imagen a Escala de grises

    public void doAction() {

//		Mat imageMat = toGray();
//		params.setMatImage(imageMat);
        toGray();

//		toGray(params.getImage());

        new UI(params).paint(null);

    }

    private void toGray() {
//	private Mat toGray() {

        Mat matImage = params.getMatImage();

//		Mat matGray = new Mat(matImage.cols(),matImage.rows(),CvType.CV_8UC1);
//		Imgproc.cvtColor(matImage, matGray, Imgproc.COLOR_RGB2GRAY);
//		return matImage;

        Imgproc.cvtColor(matImage, matImage, Imgproc.COLOR_RGB2GRAY);
    }


    // Metodo anterior
    // TRANSFORMACION A IMAGEN EN ESCALA DE GRISES
    // http://zerocool.is-a-geek.net/?p=329
    // The luminance method
    @Deprecated
    private static void toGray(BufferedImage image) {

        int alpha, red, green, blue;
        int newPixel;

        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {

                // Get pixels by R, G, B
                alpha = new Color(image.getRGB(i, j)).getAlpha();
                red = new Color(image.getRGB(i, j)).getRed();
                green = new Color(image.getRGB(i, j)).getGreen();
                blue = new Color(image.getRGB(i, j)).getBlue();

                red = (int) (0.21 * red + 0.71 * green + 0.07 * blue);
                // Return back to image format
                newPixel = ImageUtils.colorToRGB(alpha, red, red, red);

                // Write pixels into image
                image.setRGB(i, j, newPixel);
            }
        }
    }

}