package Preprocesado;

import java.awt.Color;
import java.awt.image.BufferedImage;

import Config.InitialParameters;
import org.opencv.core.*;


/**
 * Clase en la que implementamos el listener que se encarga de realizar la transformación negativa de la imagen.
 */

public class Inverse {

    private InitialParameters params;

    public Inverse(InitialParameters params) {
        this.params = params;
    }

    // Método que se encarga de la transformacion negativa (inverse)

    public void doAction() {

        inverse();

//		inverse(params.getImage());

        new UI(params).paint(null);
    }

    public void inverse() {

        Mat matImage = params.getMatImage();

        Core.bitwise_not(matImage, matImage);
    }

    @Deprecated
    public void inverse(BufferedImage image) {

        // Creamos una imagen del mismo tamaño que la original

        int w = image.getWidth();
        int h = image.getHeight();

        // http://jc-mouse.blogspot.com.es/2011/02/invertir-imagen-en-java-filtro-negativo.html

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                Color c1 = new Color(image.getRGB(i, j));

                int r = c1.getRed();
                int g = c1.getGreen();
                int b = c1.getBlue();

                image.setRGB(i, j, new Color(255 - r, 255 - g, 255 - b).getRGB());
            }
        }
    }

}
