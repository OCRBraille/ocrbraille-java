package Preprocesado;

import Config.InitialParameters;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.util.*;
import java.util.List;

import org.opencv.core.Point;

/**
 * Clase en la que implementamos el listener que se encarga de detectar los circulos de la imagen.
 */

public class DetectCircles {

    private InitialParameters params;
    //	private static int colAvgPto=0;
//	private static int colAvgLetra=0;
    public static int tam;
    private static int colAvg = 0;
    private LinkedList<Long> row, col;
    private float radiusAcum = 0;
    private Mat matImage;

    public DetectCircles(InitialParameters params) {
        this.params = params;
    }

    // Metodo se encarga de detectar los circulos

    public void doAction() {

        detectar();

        new UI(params).paint(null);

        crop();

        new UI(params).paint(null);
    }


    private void detectar() {

        double minArea = 1;
        int i, r, count = 0;
        float[] radius = new float[1];
        Point center = new Point();
//		double averR = 0, rowAvg=0, count=0;

        matImage = params.getMatImage();

        List<MatOfPoint> contours = new ArrayList<>();

        Imgproc.findContours(matImage, contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

        row = new LinkedList<>();
        col = new LinkedList<>();

        if (contours.isEmpty()) {
            try {
                throw new Exception("No se encontraron puntos en Braille");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (i = 0; i < contours.size(); i++) {
            MatOfPoint c = contours.get(i);
            minArea += Imgproc.contourArea(c);

            MatOfPoint2f c2f = new MatOfPoint2f(c.toArray());
            Imgproc.minEnclosingCircle(c2f, center, radius);
        }

        minArea = (minArea / i) * 0.31;

//		r = Math.round(radiusAcum);

        for (i = 0; i < contours.size(); i++) {
            MatOfPoint c = contours.get(i);

            MatOfPoint2f c2f = new MatOfPoint2f(c.toArray());
            Imgproc.minEnclosingCircle(c2f, center, radius);

            if (Imgproc.contourArea(c) > minArea) {

//                Core.circle(destination, center, (int)((radiusAcum+radius[0])/2), new Scalar(255, 255, 255), -2);
//                Core.circle(matImage, center, (int)(radiusAcum), new Scalar(255, 255, 255), -2);
                Core.circle(matImage, center, (int) (radius[0]), new Scalar(255, 255, 255), -2);
                radiusAcum += radius[0];
                count++;

                boolean found = false;
                r = Math.round(radius[0]);
//                averR += r;
                long x = Math.round(center.x);
                long y = Math.round(center.y);

                for (int j = 0; j < row.size(); j++) {
                    double y2 = row.get(j);
                    if (y - r < y2 && y + r > y2) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    row.addLast(y);
                }
                found = false;
                for (int j = 0; j < col.size(); j++) {
                    double x2 = col.get(j);
                    if (x - r < x2 && x + r > x2) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    col.addLast(x);
                }
//                count++;
            } else {
                Core.circle(matImage, center, (int) (radius[0] * 1.1), new Scalar(0, 0, 0), -2);
            }
        }

        radiusAcum = radiusAcum / count;
        tam = (int) (2 * radiusAcum);

//        System.out.println("" + count +" circulos");
//		averR /= count;
//		averR = Math.floor(averR);

//		row.addFirst((long) 0);
//		row.addLast((long) image.getWidth());
//
//		col.addFirst((long) 0);
//		col.addLast((long) image.getHeight());
        int x;

        Collections.sort(row);
        Collections.sort(col);

//		for (x = 0; x < row.size(); x++) {
//			center.y = row.get(x);
//			for (int y = 0; y < col.size(); y++) {
//				center.x = col.get(y);
//				Core.circle(destination, center, (int) (radiusAcum), new Scalar(255, 255, 255), -2);
//			}
//		}

//		LinkedList<Long> rowDiff = new LinkedList<>();
//		LinkedList<Long> colDiff = new LinkedList<>();
//
//		for (x = 0; x < row.size()-1; x++) {
//			rowDiff.addLast(row.get(x+1)-row.get(x));
//		}
//
//		for (x = 0; x < col.size()-1; x++) {
//			colDiff.addLast(col.get(x+1)-col.get(x));
//		}
//
//		for (x = 0; x < rowDiff.size(); x++) {
//			rowAvg += rowDiff.get(x);
//		}
//
//		rowAvg/=x+1;
//		rowAvg*=1.1;
//
//		i=0;
//
//		for (x = 0; x < colDiff.size(); x++) {
//			colAvg += colDiff.get(x);
//		}
//		colAvg/=x+1;
//
//		for (x = 0; x < colDiff.size(); x++) {
//			if(colDiff.get(x) > 0.1 * colAvg && colDiff.get(x) < 2 * colAvg)
//				colAvg += colDiff.get(x);
//		}
//
//		colAvg/=x+1;
//		colAvg*=1.1;
//
//		for ( x = 0; x < colDiff.size(); x++) {
//
//			if(colDiff.get(x) > 0.1 * colAvg && colDiff.get(x) < 3 * colAvg){
//				if( x%2 == 0 ) {
//					colAvgPto += colDiff.get(x);
//					k++;
//				}
//				else{
//					colAvgLetra += colDiff.get(x);
//					l++;
//				}
//			}
//		}
//		colAvgPto=colAvgPto/(k);
//		colAvgLetra=colAvgLetra/(l);


//		//lineas horizontales
//		for (y = ((int) (row.get(0) - (radiusAcum*2) )); y < imagen.getHeight(); y+=radiusAcum*2) {
//			Core.rectangle(destination, new Point(0,y ), new Point(image.getWidth(), y), new Scalar(255, 255, 255), -2);
//		}
//
        //lineas verticales
//		Core.rectangle(destination, new Point(col.get(0) - (radiusAcum*2), 0), new Point(col.get(0) - (radiusAcum*2), image.getHeight()), new Scalar(255, 255, 255), -2);

//		for (x = ((int) (col.get(0)+(1.5*radiusAcum))); x < imagen.getWidth(); x+=2.7*radiusAcum) {
//			Core.rectangle(destination, new Point(x, 0), new Point(x, image.getHeight()), new Scalar(255, 255, 255), -2);
//			ant=x;
//		}

//		Core.rectangle(destination, new Point(ant, 0), new Point(ant+4, image.getHeight()), new Scalar(255, 255, 255), -2);

//		params.getIndex()[0] = y1;

//		params.getSymbolHorLines().add(y1);
//		params.setTotalHorLines(params.getTotalHorLines() + 1);


//		int fila=0;
//
//		for (x = 0; x < row.size()-1; x++) {
//			i++;
//
//			//espacios
//			if (rowDiff.get(x) > rowAvg && row.size() > 3) {
//				if (i < 3 && i!=0) //TODO: SI TIENE UN SOLO PUNTO > 5 * radiusAcum
//					fila = (int) (row.get(x) + 4.5 * radiusAcum);
//
//				else
//					fila = (int) (row.get(x) + 1.5 * radiusAcum);
//
//
//				Core.rectangle(destination, new Point(0, fila), new Point(image.getWidth(), fila), new Scalar(255, 255, 255), -2);
//				params.getSymbolHorLines().add(fila);
//				params.setTotalHorLines(params.getTotalHorLines() + 1);
//
//				fila = (int) (row.get(x) + rowDiff.get(x) - 1.5 * radiusAcum);
//				Core.rectangle(destination, new Point(0, fila), new Point(image.getWidth(), fila), new Scalar(255, 255, 255), -2);
//				params.getSymbolHorLines().add(fila);
//				params.setTotalHorLines(params.getTotalHorLines() + 1);
//				i=0;
//			}
//		}
//		i=0;
//			else {
//				if(x != 0 && x % 2 == 0 && x % 3 != 0) {
//					if (rowDiff.get(x) > 2.5*radiusAcum && rowDiff.get(x) < rowAvg) {
//						Core.rectangle(destination, new Point(0, row.get(x) +3*radiusAcum+rowAvg*0.5), new Point(image.getWidth(), row.get(x) +4*radiusAcum), new Scalar(255, 255, 255), -2);
//					}
//				}
//				else if (x!= 0 && x % 3 == 0)
        //Core.rectangle(destination, new Point(0, row.get(x) + radiusAcum), new Point(image.getWidth(), row.get(x) + radiusAcum), new Scalar(255, 255, 255), -2);
//			}
//		}
//		y2= ((int) (row.get(x)+averR));


//
//		params.getIndex()[1] = y2;
//		params.getSymbolHorLines().add(y2);
//		params.setTotalHorLines(params.getTotalHorLines() + 1);

        //Core.rectangle(destination, new Point(0,row.get(x)+averR+averR/2), new Point(image.getWidth(), row.get(x)+averR+averR/2+4), new Scalar(255, 255, 255), -2);


////
//		for (x = 0; x < col.size()-1; x++) {
//			if (colDiff.get(x) > colAvg) {
//				Core.rectangle(destination, new Point(col.get(x) + radiusAcum, 0), new Point(col.get(x) + radiusAcum, image.getHeight()), new Scalar(255, 255, 255), -2);
//				Core.rectangle(destination, new Point(col.get(x) + colDiff.get(x)-radiusAcum, 0), new Point(col.get(x) + colDiff.get(x)- radiusAcum, image.getHeight()), new Scalar(255, 255, 255), -2);
//			}
//			else{
//				Core.rectangle(destination, new Point(col.get(x) + radiusAcum, 0), new Point(col.get(x) + radiusAcum, image.getHeight()), new Scalar(255, 255, 255), -2);
//			}
//		}
//		x2= ((int) (col.get(x)+averR));

//		int columna=0;
//
//		for (x = 0; x < col.size()-1; x++) {
//			i++;
//
//			//espacios
//
//			columna = (int) (col.get(x) + 1.5 * radiusAcum);
//
//			if (colDiff.get(x) > colAvg) {
//				columna = (int) (colDiff.get(x) - 1.5 * radiusAcum);
//				Core.rectangle(destination, new Point(columna, 0), new Point(columna, image.getHeight()), new Scalar(255, 255, 255), -2);
//				params.getSymbolHorLines().add(columna);
//
//				if (x!=0 && x%2 !=0)
//					columna = (int) (col.get(x) + 4.5 * radiusAcum);
//			}
//
//			Core.rectangle(destination, new Point(columna, 0), new Point(columna, image.getHeight()), new Scalar(255, 255, 255), -2);
//			params.getSymbolHorLines().add(columna);
//		}


//		Rect roi = new Rect(imagen.getHeight()-y2 ,x1, imagen.getHeight()-y1,x2);
//		Mat dest = new Mat(destination, roi);

//		double row[] = new double[20];
//		double col[] = new double[20];


        //Find rows and columns of circles for interpolation
//		for(int i=0;i< contours.size();i++){
//
//		}

//		for (int x = 0; x < circles.cols(); x++) {
//			double vCirculo[] = circles.get(0, x);
//
//			if (vCirculo == null) {
//				break;
//			}
//
//			pnt = new Point(Math.round(vCirculo[0]), Math.round(vCirculo[1]));
//			radio = (int) Math.round(vCirculo[2]);
//
//			Core.circle(destination, pnt, radio, new Scalar(255, 255, 255), 3);
//			Core.circle(destination, pnt, 1, new Scalar(255, 0, 0), 3);
//		}


        // Recorremos dicho vector de líneas horizontales

//		for(i = 0; i < params.getSymbolHorLines().size() - 1 ; i++){
//
//			// Calculamos el rango para cada dos líneas horizontales
//
//			int rango = params.getSymbolHorLines().get(i+1) - params.getSymbolHorLines().get(i);
//
//			// Pintamos las dos líneas correspondientes (para dividir el carácter Braille en 3 filas) y las añadimos en la lista de líneas verticalesBraille
//			Core.rectangle(destination, new Point(0,params.getSymbolHorLines().get(i) + rango/3), new Point(image.getWidth(), params.getSymbolHorLines().get(i) + rango/3), new Scalar(255, 255, 255), -2);
//			params.getDotHorLines().add(params.getSymbolHorLines().get(i) + rango/3);
//
//			Core.rectangle(destination, new Point(0,params.getSymbolHorLines().get(i) + 2*(rango/3)), new Point(image.getWidth(), params.getSymbolHorLines().get(i) + 2*(rango/3)), new Scalar(255, 255, 255), -2);
//			params.getDotHorLines().add(params.getSymbolHorLines().get(i) + 2*(rango/3));
//
//			params.setTotalHorLines(params.getTotalHorLines() + 2);
//			i++;
//		}


    }


    private void crop() {

        int y, ant = 0, x1 = 0, x2 = 0, y1 = 0, y2 = 0, k = 0, l = 0;

        if (row.isEmpty() || col.isEmpty()) {
            try {
                throw new Exception("No se encontraron puntos en Braille");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //arriba

        y1 = ((int) (row.get(0) - (radiusAcum)));

        if (y1 <= 0)
            y1 = 1;

        Core.rectangle(matImage, new Point(0, y1), new Point(matImage.cols(), y1), new Scalar(255, 255, 255), -2);

        //abajo

        y2 = ((int) (row.get(row.size() - 1) + (radiusAcum)));

        if (y2 > matImage.rows())
            y2 = matImage.rows() - 1;

        Core.rectangle(matImage, new Point(0, y2 - 1), new Point(matImage.cols(), y2 - 1), new Scalar(255, 255, 255), -2);

        //izquierda
//		TODO: ver espacio para mayuscula
        x1 = ((int) (col.get(0) - radiusAcum * 3.5));

//		x1= ((int) (col.get(0)-radiusAcum*0.9));
//		x1= ((int) (col.get(0)-3*radiusAcum));

        if (x1 <= 0)
            x1 = 1;

//        params.getIndex()[2] = x1;
        Core.rectangle(matImage, new Point(x1, 0), new Point(x1, matImage.rows()), new Scalar(255, 255, 255), -2);

        //derecha
        x2 = ((int) (col.get(col.size() - 1) + (radiusAcum * 4)));

        if (x2 > matImage.cols())
            x2 = matImage.cols() - 1;

//        params.getIndex()[3] = x2;
        Core.rectangle(matImage, new Point(x2 - 1, 0), new Point(x2 - 1, matImage.rows()), new Scalar(255, 255, 255), -2);

        Rect roi = new Rect(x1, y1, x2 - x1, y2 - y1);
        Mat dest = new Mat(matImage, roi);
        params.setMatImage(dest);

    }

}

