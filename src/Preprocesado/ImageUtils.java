package Preprocesado;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

/**
 * Clase Utils en la que implementamos metodos utilizados en las operaciones que realizamos a la imagen.
 */

public class ImageUtils {

    // Metodo utilizado en la transformación RGB-Escala de grises y en la binarización
    // http://zerocool.is-a-geek.net

    public static int colorToRGB(int alpha, int red, int green, int blue) {

        int newPixel = 0;
        newPixel += alpha;
        newPixel = newPixel << 8;
        newPixel += red;
        newPixel = newPixel << 8;
        newPixel += green;
        newPixel = newPixel << 8;
        newPixel += blue;

        return newPixel;
    }

    //conversion de Mat a Imagen

    public static BufferedImage matToImage(Mat matImage) {

        byte[] data = new byte[matImage.rows() * matImage.cols() * (int) (matImage.elemSize())];
        matImage.get(0, 0, data);
        BufferedImage output = new BufferedImage(matImage.cols(), matImage.rows(), BufferedImage.TYPE_BYTE_GRAY);
        output.getRaster().setDataElements(0, 0, matImage.cols(), matImage.rows(), data);

        return output;
    }

    //conversion de Imagen a Mat

    public static Mat imageToMat(BufferedImage image) {

        byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        Mat matImage = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3);
        matImage.put(0, 0, data);

        return matImage;
    }

    // Metodo que realiza una copia de la imagen que se le pasa como parámetro

    public static BufferedImage copiaImagen(BufferedImage original) {
        BufferedImage salida = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());

        for (int i = 0; i < original.getWidth(); i++) {
            for (int j = 0; j < original.getHeight(); j++) {
                salida.setRGB(i, j, original.getRGB(i, j));
            }
        }

        return salida;
    }

    public static BufferedImage copiaImagen(BufferedImage original, int ancho, int alto) {
        BufferedImage salida = new BufferedImage(ancho, alto, original.getType());

        for (int i = 0; i < original.getWidth(); i++) {
            for (int j = 0; j < original.getHeight(); j++) {
                salida.setRGB(i, j, original.getRGB(i, j));
            }
        }

        return salida;
    }

}